#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb_image_resize.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include <algorithm>
#include <cstdlib>
#include <string>
#include <memory>
#include <vector>
#include <dirent.h>
#include <sys/stat.h>
#include <queue>

using namespace std;

struct Image {
  unsigned char* p;
  int w, h, n;

  Image() :
      p(nullptr), w(0), h(0), n(0) {
  }

  void print() const {
    printf("w=%d h=%d n=%d\n", w, h, n);
  }
};

Image load(const string& s) {
  Image ret;
  ret.p = stbi_load(s.c_str(), &ret.w, &ret.h, &ret.n, 0);
  return ret;
}

Image resize(const Image& img, int w, int h) {
  Image ret;
  ret.p = (unsigned char*)malloc(sizeof(unsigned char) * w * h * img.n);
  ret.w = w;
  ret.h = h;
  ret.n = img.n;
  stbir_resize_uint8(img.p, img.w, img.h, 0,
                     ret.p, ret.w, ret.h, 0, img.n);
  return ret;
}

Image clip(const Image& img, int x, int y, int w, int h) {
  Image ret;
  ret.p = (unsigned char*)malloc(sizeof(unsigned char) * w * h * img.n);
  ret.w = w;
  ret.h = h;
  ret.n = img.n;
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      for (int k = 0; k < img.n; k++) {
        ret.p[i*ret.w*ret.n + j*ret.n + k] = img.p[(y+i)*img.w*img.n + (x+j)*img.n + k];
      }
    }
  }
  return ret;
}

Image pnot(const Image& img) {
  Image ret;
  ret.p = (unsigned char*)malloc(sizeof(unsigned char) * img.w * img.h * img.n);
  ret.w = img.w;
  ret.h = img.h;
  ret.n = img.n;
  for (int i = 0; i < img.h; i++) {
    for (int j = 0; j < img.w; j++) {
      for (int k = 0; k < img.n; k++) {
        if (k == 3) {
          ret.p[i*img.w*img.n + j*img.n + k] = img.p[i*img.w*img.n + j*img.n + k];
        } else {
          ret.p[i*img.w*img.n + j*img.n + k] = 255-img.p[i*img.w*img.n + j*img.n + k];
        }
      }
    }
  }
  return ret;
}

int save(const Image& img, const string& s) {
  return stbi_write_png(s.c_str(), img.w, img.h, img.n, img.p, img.n*img.w);
}

double calcLuminance(int r, int g, int b) {
  return 0.298912 * r +
      0.586611 * g +
      0.114478 * b;
}

vector<double> luminance(const Image& img) {
  vector<double> ret;
  for (int i = 0; i < img.h; i++) {
    for (int j = 0; j < img.w; j++) {
      ret.push_back(calcLuminance(img.p[i*img.w*img.n + j*img.n + 0],
                                  img.p[i*img.w*img.n + j*img.n + 1],
                                  img.p[i*img.w*img.n + j*img.n + 2]));
    }
  }
  return ret;
}

Image grayscale(const Image& img) {
  Image ret;
  ret.p = (unsigned char*)malloc(sizeof(unsigned char) * img.w * img.h * img.n);
  ret.w = img.w;
  ret.h = img.h;
  ret.n = img.n;
  for (int i = 0; i < img.h; i++) {
    for (int j = 0; j < img.w; j++) {
      int l = max(0,
                  min(255,
                      int(calcLuminance(img.p[i*img.w*img.n + j*img.n + 0],
                                        img.p[i*img.w*img.n + j*img.n + 1],
                                        img.p[i*img.w*img.n + j*img.n + 2]))));
      for (int k = 0; k < img.n; k++) {
        if (k < 3) {
          ret.p[i*img.w*img.n + j*img.n + k] = l;
        } else {
          ret.p[i*img.w*img.n + j*img.n + k] = 255;
        }
      }
    }
  }
  return ret;
}

template<class T> T sq(const T& a) { return a*a; }

double calcDiff(const Image& a, const Image& b, int x, int y) {
  double res = 0;
  for (int i = 0; i < b.h; i++) {
    for (int j = 0; j < b.w; j++) {
      res += sq(a.p[(i+y)*a.n*a.w+a.n*(j+x)] - b.p[i*b.n*b.w+b.n*j]);
    }
  }
  return res;
}

void copy(Image& a, const Image& b, int x, int y) {
  for (int i = 0; i < b.h; i++) {
    for (int j = 0; j < b.w; j++) {
      for (int k = 0; k < b.n; k++) {
        a.p[(i+y)*a.w*a.n + (j+x)*a.n + k] = b.p[i*b.w*b.n + j*b.n + k];
      }
      if (b.n == 3 && a.n == 4) {
        a.p[(i+y)*a.w*a.n + (j+x)*a.n + 3] = 255;
      }
    }
  }
}

struct edge {
  int to, cap;
  double cost;
  int rev, x;
};
vector<vector<edge>> G;
typedef pair<int,int> P;
const double eps = 1e-9;
vector<int> h, dist, prevv, preve;
const int inf = 1<<28;
void add_edge(int f,int t,int cap,double cost, int x=-1)
{
  G[f].push_back((edge){t,cap,cost,(int)G[t].size(),x});
  G[t].push_back((edge){f,0,-cost,(int)G[f].size()-1,x});
}
double min_cost_flow(int s,int t,int f)
{
  double res = 0;
  fill(h.begin(), h.end(), 0);
  while( f > 0 ) {
    priority_queue<P,vector<P>,greater<P> > q;
    fill(dist.begin(),dist.end(),inf);
    dist[s]=0;
    q.push(P(0,s));
    while(!q.empty()) {
      P p = q.top(); q.pop();
      int v = p.second;
      if( dist[v] < p.first ) continue;
      for( int i = 0; i < int(G[v].size()); i++ ) {
        edge& e = G[v][i];
        if( e.cap > 0 && dist[e.to] > dist[v]+e.cost+h[v]-h[e.to]+eps ) {
          dist[e.to] = dist[v]+e.cost+h[v]-h[e.to];
          prevv[e.to] = v;
          preve[e.to] = i;
          q.push(P(dist[e.to],e.to));
        }
      }
    }
    if( abs(dist[t]-inf)<eps ) {
      return -1;
    }
    for(size_t v = 0; v < h.size(); v++) h[v] += dist[v];
    int d = f;
    for( int v = t; v != s; v = prevv[v] ) {
      d = min(d,G[prevv[v]][preve[v]].cap);
    }
    f -= d;
    res += d*h[t];
    for( int v = t; v != s; v = prevv[v] ) {
      edge &e = G[prevv[v]][preve[v]];
      e.cap -= d;
      G[v][e.rev].cap += d;
    }
  }
  return res;
}

int main(void) {
  const int W = 4;
  const int H = 3;
  const int SW = 16; // W*10
  const int SH = 15; // H*10

  Image src = resize(load("unko/t.png"), W*100, H*100);
  Image gsrc = grayscale(src);
  vector<Image> is, gs;
  dirent* entry;
  const string path = "./test/";
  DIR* dp = opendir(path.c_str());
  
  puts("loading");
  do {
    entry = readdir(dp);
    if (entry != NULL) {
      if (string(entry->d_name).find(".png") != string::npos ||
          string(entry->d_name).find(".jpg") != string::npos) {
        printf("load %s\n", entry->d_name);
        Image i = load(path + entry->d_name);
        if (i.p == nullptr) continue;
        is.push_back(clip(resize(i, SW+40, SH+40),
                       20, 20, SW, SH));
        gs.push_back(grayscale(is.back()));
      }
    }
  } while (entry != NULL);
  closedir(dp);
  puts("done");

  const int WW = src.w/SW;
  const int HH = src.h/SH;

  G.resize(WW*HH*2 + is.size() + 2);
  h.resize(WW*HH*2 + is.size() + 2);
  dist.resize(WW*HH*2 + is.size() + 2);
  prevv.resize(WW*HH*2 + is.size() + 2);
  preve.resize(WW*HH*2 + is.size() + 2);

  for (int i = 0; i < HH; i++) {
    for (int j = 0; j < WW; j++) {
      add_edge(is.size()+1+i*WW+j, is.size()+1+WW*HH+i*WW+j, 1, 0);
      add_edge(is.size()+1+WW*HH+i*WW+j, is.size()+1+WW*HH*2, 1, 0);
    }
  }

  for (size_t ii = 0; ii < is.size(); ii++) {
    printf("%4zd/%4zd calculating...\n", ii+1, is.size());
    add_edge(0, ii+1, 1, 0);
    for (int i = 0; i < HH; i++) {
      for (int j = 0; j < WW; j++) {
        double ret = calcDiff(gsrc, gs[ii], j*SW, i*SH);
        add_edge(ii+1, is.size()+1+i*WW+j, 1, ret, ii);
      }
    }
  }

  double res = min_cost_flow(0, WW*HH*2+is.size()+1, WW*HH);
  printf("%.10f\n", res);
  int cnt = 0;
  for (size_t ii = 0; ii < is.size(); ii++) {
    for (auto&& e : G[ii+1]) {
      if (e.cap == 0 && e.x >= 0) {
        ++cnt;
        int t = e.to-is.size()-1;
        copy(src, is[ii], (t%WW)*SW, (t/WW)*SH);
      }
    }
  }
  save(src, "res.png");
  return 0;
}
